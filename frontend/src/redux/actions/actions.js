import { ADD_PRODUCT_TO_CART } from './actionTypes'
import { REMOVE_PRODUCT_FROM_CART } from './actionTypes'
import { UPDATE_PRODUCT_IN_CART } from './actionTypes'
import { SET_FLASH } from './actionTypes'

export const addProductToCart = (quantity, product) => ({
  type: ADD_PRODUCT_TO_CART,
  payload: {
    item: quantity, product
  }
})

export const removeProductFromCart = (quantity, product) => ({
  type: REMOVE_PRODUCT_FROM_CART,
  payload: {
    item: quantity, product
  }
})

export const updateProductInCart = (quantity, product) => ({
  type: UPDATE_PRODUCT_IN_CART,
  payload: {
    item: quantity, product
  }
})

export const setFlash = ({ message, success, visible }) => ({
    type: SET_FLASH,
    message,
    success,
    visible: visible === undefined ? true : visible
})
