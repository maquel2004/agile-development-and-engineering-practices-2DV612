import { ADD_PRODUCT_TO_CART } from '../actions/actionTypes'
import { REMOVE_PRODUCT_FROM_CART } from '../actions/actionTypes'
import { UPDATE_PRODUCT_IN_CART } from '../actions/actionTypes'
import produce from 'immer'

const initialState = {
  open: true,
  test: 'hej',
  products: [],
  total: 0
}

const CartReducer = produce((state = initialState, action) => {
  switch (action.type) {
    case ADD_PRODUCT_TO_CART: {
      const prods = state.products

      const product = {
        id: action.payload.item.product.id,
        quantity: action.payload.item.quantity,
        product: action.payload.item.product
      }

      const found = prods.find(element => element.id === product.id)

      if (found) {
        const quantity = parseFloat(found.quantity) + parseFloat(product.quantity)
        found.quantity = quantity
      } else {
        prods.push(product)
      }

      state.total += parseFloat(product.quantity) * parseFloat(product.product.price)

      break;
    }
    case REMOVE_PRODUCT_FROM_CART: {
      const product = {
        id: action.payload.item.product.id,
        quantity: action.payload.item.quantity,
        product: action.payload.item.product
      }
      state.products = state.products.filter(element => element.id !== action.payload.item.product.id)
      state.total -= product.quantity * product.product.price

      break;
    }
    case UPDATE_PRODUCT_IN_CART: {
      const product = {
        id: action.payload.item.product.id,
        quantity: action.payload.item.quantity,
        product: action.payload.item.product
      }

      const prods = state.products
      const found = prods.find(element => element.id === product.id)

      
      state.total += (product.quantity - found.quantity) * product.product.price
      found.quantity = product.quantity

      break;
    }
    default:
      return state
  }
})

export default CartReducer