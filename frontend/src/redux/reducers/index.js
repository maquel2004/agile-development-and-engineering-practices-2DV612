import { combineReducers } from "redux"
import CartReducer from "./CartReducer"
import FlashReducer from "./FlashReducer"

export default combineReducers({ shoppingCart: CartReducer, flash: FlashReducer })