import { SET_FLASH } from "redux/actions/actionTypes"

const initialState = {
  message: '',
  success: false,
  visible: false
}

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_FLASH: {
      return {
        ...state,
        message: action.message,
        success: action.success,
        visible: action.visible
      }
    }
    default:
      return state
  }
}