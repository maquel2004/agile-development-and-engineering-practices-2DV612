import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch, faBox} from '@fortawesome/free-solid-svg-icons'
import './FruityButton.css'

const getIcon = (icon) => {
  switch(icon) {
    case 'search':
      return faSearch
     default:
       return faBox
  }
}

const FruityButton = ({text, icon}) =>
  <button className="FruityButton">{text}<FontAwesomeIcon icon={getIcon(icon)} className='Icon'/></button>

export default FruityButton