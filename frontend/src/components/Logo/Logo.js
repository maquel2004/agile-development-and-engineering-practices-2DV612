import React from 'react'
import { NavLink } from 'react-router-dom'
import './Logo.css'


const Logo = (props) =>
    <div className="Logo">
      <NavLink to='/' className="LogoLink">
        <span>FRUKT&&SÅNT</span>
      </NavLink>
    </div>

export default Logo