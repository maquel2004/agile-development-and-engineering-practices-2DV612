import React, { useEffect, useState } from 'react'
import FruityButton from 'components/FruityButton'
import './SearchField.css'
import { Redirect } from 'react-router-dom'

const SearchField = () => {
  const [redirect, setRedirect] = useState(false)
  const [input, setInput] = useState('')
  
  useEffect(() => {
    if (redirect) {
      setRedirect(false)
      setInput('')
    }
  }, [redirect])

  const onChange = (e) => {
    setInput(e.target.value)
  }

  const ifEnter = (e) => {
    if (e.code !== 'Enter') return
    handleSubmit()
    e.preventDefault()
  }
  const handleSubmit = () => {
    setRedirect(true)
  }
  return (
    <div className="SearchField">
      {redirect && <Redirect to={`/search/products/${input}`} />}
      <div className="SelectBox">
        <select>
          <option value="Alla kategorier">Alla kategorier</option>
          <option value="">Frukt</option>
          <option value="">Grönsaker</option>
        </select>
      </div>
      <input
        placeholder="Sök efter produkt.."
        id="productSearchField"
        value={input}
        onChange={onChange}
        onKeyPress={ifEnter}
      ></input>
      <FruityButton onClick={handleSubmit} icon="search"/>
    </div>
  )  
}

export default SearchField