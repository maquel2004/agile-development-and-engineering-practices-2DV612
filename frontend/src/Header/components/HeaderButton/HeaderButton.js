import React from 'react'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShoppingCart, faUser, faBars, faBox } from '@fortawesome/free-solid-svg-icons'
import './HeaderButton.css'


const getIcon = (icon) => {
  switch(icon) {
    case 'cart':
      return faShoppingCart
    case 'user':
      return faUser
    case 'menu':
      return faBars
    default:
      return faBox
  }
}

const HeaderButton = ({ icon, name, open, click }) => {

  const bgColor = open ? 'Open' : ''

  const gridArea = icon + ' '

  return(
    <button className={'HeaderButton ' + gridArea + bgColor} onClick={() => click(!open)}>
      <FontAwesomeIcon icon={getIcon(icon)} className='HeaderIcon'/>
      <span>{name}</span>
    </button>
  )
}

HeaderButton.propTypes = {
  icon: PropTypes.string,
  name: PropTypes.string,
  open: PropTypes.bool,
  click: PropTypes.func
}

export default HeaderButton