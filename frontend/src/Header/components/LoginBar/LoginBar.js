import React, { Component } from 'react';
import { useState, useEffect } from 'react'
import { Route } from "react-router-dom";
import axiosInstance from '../../../auth-common';
import './LoginBar.css'


class LoginBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            currentUser: {},
            hiddenLoginForm: this.isUserLoggedIn(),
            hiddenAlreadySignedIn: !this.isUserLoggedIn(),
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleLogout = this.handleLogout.bind(this);
    }

    isUserLoggedIn() {
        if (localStorage.getItem('access_token') == null) {
            return false;
        } else {
            return true;
        }
    }

    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    async handleSubmit(event) {
        event.preventDefault();
        try {
            const response = await axiosInstance.post('/token/create', {
                username: this.state.username,
                password: this.state.password
            });

            axiosInstance.defaults.headers['Authorization'] = "JWT " + response.data.access;
            localStorage.setItem('access_token', response.data.token);
            localStorage.setItem('refresh_token', response.data.refresh);
            this.setState({ hiddenLoginForm: !this.state.hiddenLoginForm });
            this.setState({ hiddenAlreadySignedIn: !this.state.hiddenAlreadySignedIn });

            // Gets current user and sets the object in session storage
            try  {
                const response = await axiosInstance.get('/current_user/', {
                    headers: {
                        Authorization: `JWT ${localStorage.getItem('access_token')}`
                      }
                });
                sessionStorage.setItem('user_data', JSON.stringify(response.data));
                this.setState({ currentUser: response.data });
            } catch (error) {
                throw error;
            }
            
            return response;
        } catch (error) {
            alert('Username or password is incorrect!');
        }
    }

    handleLogout(event) {
        event.preventDefault();
        localStorage.removeItem('access_token');
        localStorage.removeItem('refresh_token');
        sessionStorage.removeItem('user_data');
        axiosInstance.defaults.headers['Authorization'] = null;
        this.setState({ hiddenAlreadySignedIn: !this.hiddenAlreadySignedIn });
        this.setState({ hiddenLoginForm: this.hiddenLoginForm });
    };

    render() {
        return (
        <div className='LoginBar'>
            <div className="base-container">
                <div className="content">
                    
                    <div id="login-form" className={this.state.hiddenLoginForm ? 'hidden' : ''}>
                        Logga in
                        <form onSubmit={this.handleSubmit}>
                            <label>
                                Användarnamn:
                                <input name="username" type="text" value={this.state.username} onChange={this.handleChange}/>
                            </label>
                            <label>
                                Lösenord:
                                <input name="password" type="password" value={this.state.password} onChange={this.handleChange}/>
                            </label>
                            <input type="submit" value="Logga in"/>
                        </form>
                        <p>Inget konto ännu? <a href="/signup">Registrera dig här.</a></p>
                    </div>

                    <div id="logged-in" className={this.state.hiddenAlreadySignedIn ? 'hidden' : ''}>
                        Välkommen {this.state.currentUser.username} till Frukt&&Sånt! 
                        <button id="logout" onClick={this.handleLogout}>Logga ut</button>
                    </div>

                </div>
            </div>
        </div>
        )
    }
}

export default LoginBar