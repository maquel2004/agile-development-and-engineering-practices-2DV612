import React from 'react'

import './CartProductImage.css'

const CartProductImage = ({image, name}) =>
  <img src={image} className='CartProductImage' alt={name} />

export default CartProductImage