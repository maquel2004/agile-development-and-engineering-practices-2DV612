import React from 'react'

import './CartProductQuantityPrice.css'
import { updateProductInCart } from 'redux/actions/actions'
import { connect } from 'react-redux'

const CartProductQuantityPrice = ({item, updateProductInCart}) => {

  const product = item.product

  const handleChange = (e) => {
    handleUpdate(e.target.value)
  }

  const handleUpdate = quantity => {
    updateProductInCart({ quantity, product })
  }

  return(
    <div className='CartProductQuantityPrice'>
      <input id='CartProductQuantity' className='CartProductQuantityPicker' type='number' value={item.quantity} min='1' onChange={handleChange}></input>
      <span className='CartProdPrice'>{item.product.price * item.quantity} kr</span>
    </div>
  )
}

const mapDispatchToProps = { updateProductInCart }

export default connect(
  null,
  mapDispatchToProps
)(CartProductQuantityPrice)