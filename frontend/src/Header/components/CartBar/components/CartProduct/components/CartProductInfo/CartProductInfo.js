import React from 'react'
import { NavLink } from 'react-router-dom'

import './CartProductInfo.css'

import CartProductTrashCan from '../CartProductTrashCan'

const CartProductInfo = ({item, action}) =>
  <div className='CartProductInfo'> 
    <div className='CartProductName'>
      <span>{item.product.name}</span>
      <span className='CartProdDesc'>{item.product.description}</span>
    </div>
    <CartProductTrashCan action={action} quantity={item.quantity}/>
  </div>

export default CartProductInfo