import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash } from '@fortawesome/free-solid-svg-icons'

import './CartProductTrashCan.css'

const CartProductTrashCan = ({action, quantity}) =>
  <button className='CartProductTrashCan' onClick={() => { console.log('remove', quantity) 
  action(quantity)}}><FontAwesomeIcon icon={faTrash} className='TrashIcon'/></button>

export default CartProductTrashCan