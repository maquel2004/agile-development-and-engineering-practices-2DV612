import React from 'react'

import './CartProduct.css'

import CartProductImage from './components/CartProductImage'
import CartProductInfo from './components/CartProductInfo'
import CartProductQuantityPrice from './components/CartProductQuantityPrice'
import { removeProductFromCart } from 'redux/actions/actions'
import { connect } from 'react-redux'

const CartProduct = ({ item, removeProductFromCart }) => {

  const product = item.product

  const handleRemoveProduct = quantity => {
    removeProductFromCart({ quantity, product })
  }

  return(
    <li className='CartProduct' key={item.product.id}>
      <CartProductImage image={item.product.image} name={item.name} />
      <div className='CartProd'>
        <CartProductInfo item={item} action={handleRemoveProduct}/>
        <CartProductQuantityPrice item={item} />
      </div>
    </li>
  )
}
  

const mapDispatchToProps = { removeProductFromCart }

export default connect(
  null,
  mapDispatchToProps
)(CartProduct)