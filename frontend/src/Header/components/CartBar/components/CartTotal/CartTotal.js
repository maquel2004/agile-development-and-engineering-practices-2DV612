import React from 'react'

import './CartTotal.css'

const CartTotal = ({total}) =>
  <div className='CartTotal'>
    <span>Summa inkl moms</span><span>{total} kr</span>
  </div>

export default CartTotal