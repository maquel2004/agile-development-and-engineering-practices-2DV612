import React from 'react'
import { NavLink } from 'react-router-dom'

import './CartCheckoutButton.css'

const CartCheckoutButton = () =>
  <NavLink to='/checkout'>
    <button className='CartCheckoutButton'>Till kassan</button>
  </NavLink>
  

export default CartCheckoutButton