import './CartBar.css'
import { connect } from 'react-redux'
import CartProduct from './components/CartProduct'
import CartTotal from './components/CartTotal'
import CartCheckoutButton from './components/CartCheckoutButton'

const CartBar = ({ products, total }) =>
  {
    const prods = products
    return(
      <div className='CartBar'>
        {!prods.length > 0 && 'Inga produkter i kundvagnen.'}
        <ul>
          {prods && prods.map(item => 
            <CartProduct item={item} key={item.id} />
          )}
        </ul>
        <CartTotal total={total}/>
        {prods.length > 0 && <CartCheckoutButton /> }
      </div>
    )
  }
  

const mapStateToProps = (state) => {
  return {
    open: state.shoppingCart.open,
    products: state.shoppingCart.products,
    total: state.shoppingCart.total
  }
}

export default connect(
  mapStateToProps
)(CartBar)