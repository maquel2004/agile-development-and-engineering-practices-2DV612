import './NavBar.css'
import { NavLink } from 'react-router-dom'
import { useState, useEffect } from 'react'
import CategoriesService from 'services/CategoriesService'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAppleAlt, faShoppingBag, faTimes } from '@fortawesome/free-solid-svg-icons'

const categories = [
    {name: 'Frukt', path: '/frukt'},
    {name: 'Grönsaker', path: '/gronsaker'},
    {name: 'Kött', path: '/kott'}
]
const producers = [{name: 'Växjö Frukt & Grönt', path: '/vfg'}, {name: 'Kalmar Kött', path: '/kk'}, {name: 'Olofström Ost', path: '/oo'}]

const getIcon = (icon) => {
  switch(icon) {
    case 'Frukt':
      return faAppleAlt
    case 'Grönsaker':
      return faAppleAlt
    default:
      return faShoppingBag
  }
}

const NavBar = () => {
    const [categories, setCategories] = useState([])

    useEffect(() => {
        retrieveCategories()
      }, [])

  const retrieveCategories = async () => {
    const response = await CategoriesService.getAll()
    const cats = response.data.results
    const catSorted  = cats.sort((a, b) => a.created - b.created)
    setCategories(catSorted)
  }

return (
      <div className='NavBar'>
        <ul>
            {categories && categories.map(item =>
                <NavLink to={'/categories/' + item.name} className='MenuLink' key={'Link' + item.name}>
                    <li key={item.name}>
                    <FontAwesomeIcon icon={getIcon(item.name)} className='MenuIcon'/> {item.name}</li>
                </NavLink>
            )}
        </ul>
        <span className='MenuHeader'>
          <NavLink to='/MyProducts/' className='MenuLink' key='Link'>Våra produkter</NavLink>
        </span>
        <span></span>
        <span className='MenuHeader'>
          Våra producenter
        </span>
      </div>
      )
}
export default NavBar