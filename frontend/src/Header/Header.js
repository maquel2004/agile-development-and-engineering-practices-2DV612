import React, { useState } from 'react'
import './Header.css'
import Logo from 'components/Logo'
import SearchField from './components/SearchField'
import HeaderButton from './components/HeaderButton'
import NavBar from './components/NavBar'
import LoginBar from './components/LoginBar'
import CartBar from './components/CartBar'

const Header = () => {

  const [openNavBar, setNavBarOpen] = useState(false)
  const [openLoginBar, setLoginBarOpen] = useState(false)
  const [openCartBar, setCartBarOpen] = useState(false)

  return(
    <div className="Header">
      <HeaderButton name="MENY" icon='menu' open={openNavBar} click={setNavBarOpen} />
      <Logo />
      <SearchField />
      <HeaderButton name='LOGGA IN' icon='user' open={openLoginBar} click={setLoginBarOpen} />
      <HeaderButton name='KUNDVAGN' icon='cart' open={openCartBar} click={setCartBarOpen} />
      {openNavBar && <NavBar />}
      {openLoginBar && <LoginBar />}
      {openCartBar && <CartBar />}
  </div>
  )
}
  
export default Header