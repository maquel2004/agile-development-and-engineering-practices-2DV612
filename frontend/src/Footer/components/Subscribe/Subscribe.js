import React from 'react'
import './Subscribe.css'

const Subscribe = () =>
  <div className="Subscribe">
    <form>
      <span>Nyhetsbrev</span>
      <input placeholder="exempel@exempel.com"/>
      <button type="submit">Skicka</button>
    </form>
  </div>

export default Subscribe

