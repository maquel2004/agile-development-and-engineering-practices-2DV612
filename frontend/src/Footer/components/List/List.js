import React from 'react'
import './List.css'

const List = ({children, name }) =>
  <div className="List">
     {name && <span>{name}</span>}
    <ul>
      {children}
    </ul>
  </div>

export default List

