import React from 'react'
import './ListItem.css'

const ListItem = ({ name, url }) =>
  <li className="ListItem">
    <a href={url}>{name}</a>
  </li>


export default ListItem

