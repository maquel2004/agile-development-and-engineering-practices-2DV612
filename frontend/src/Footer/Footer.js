import React from 'react'
import './Footer.css'
import List from './components/List'
import ListItem from './components/ListItem'
import Logo from 'components/Logo'
import Subscribe from './components/Subscribe'


const Footer = () => 
  <div className='Footer'>
    <Logo />

    <List name='Mitt konto'>
      <ListItem name='Logga in' url='/login'/>
      <ListItem name='Orderhistorik' url='/orderhistorik'/>
      <ListItem name='Önskelistor' url='/onskelista'/> 
      <ListItem name='Bevakningar' url='/bevakningar'/> 
      <ListItem name='Kunduppgifter' url='/kunduppgifter'/> 
      <ListItem name='Personuppgifter' url='/personuppgifter'/> 
    </List>

    <List name='Kundservice'>
      <ListItem name='Kontakta oss' url='/kontaktaoss'/>
      <ListItem name='Köpvillkor' url='/villkor'/>
      <ListItem name='Vanliga frågor & svar' url='/faq'/> 
    </List>
    
    <List name='Följ oss'>
      <ListItem name='Facebook' url='https://facebook.com'/>
      <ListItem name='Instagram' url='https://instagram.com'/>
      <ListItem name='Twitter' url='https://twitter.com'/> 
    </List>

    <List>
      <ListItem name='Om oss' url="/myproducts" />
      <ListItem name='Butiker' url="google.com" />
    </List>
    
    <Subscribe />
  </div>

export default Footer