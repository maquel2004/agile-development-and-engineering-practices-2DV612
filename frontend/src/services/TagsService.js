import http from '../http-common'
import { API_ENDPOINT } from '../config/url'

const getAll = () => {
  return http.get('/tags/')
}

const getById = (id) => {
  return http.get(`/tags/${id}/`)
}

const getFromUrl = (url) => {
  return http.get(url)
}

const search = (param) => {
  return http.get(`/search-tags/?search=${param}`)
}

const getExact = param => {
  return http.get(`/search-tags-exact/?search=${param}`)
}

const getAPILinkOfTagName = async (name) => {
  console.log('This is from get api link')
  const data = await http.get(`/search-tags-exact/?search=${name}`)
                         .then(res => res.data)

  if (data.count < 1) {
    return 'Tag does not exist'
  } else {
    console.log(data.results)
    return `${API_ENDPOINT}/tags/${data.results[0].id}/`
  }
}

const create = (data) => {
  return http.post('/tags/', data)
}

export default {
  getAll,
  getById,
  getFromUrl,
  search,
  getExact,
  getAPILinkOfTagName,
  create
}