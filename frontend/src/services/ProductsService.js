import http from '../http-common'

const getAll = () => {
  return http.get('/products/')
}

const getPage = (page) => {
  return http.get(`/products/?page=${page}`)
}

const getById = id => {
  return http.get(`/products/${id}/`)
}

const create = data => {
  return http.post('/products/', data)
}

const update = (data) => {
  console.log(data)
  return http.put(`/products/${data.id}/`, data)
}

const search = data => {
  return http.get(`/search-products/?search=${data}`)
}

/**
 * Search for products with tag name or id
 */
const getByTag = tag => {
  return http.get(`/search-products-by-tag/?search=${tag}`)
}

const getByCategory = category => {
  return http.get(`/search-products-by-category/?search=${category}`)
}

const ProductsService = {
  getAll,
  getPage,
  getById,
  create,
  update,
  search,
  getByTag,
  getByCategory
}

export default ProductsService