import http from '../http-common'

const getAll = () => {
  return http.get('/categories/')
}

const getById = id => {
  return http.get(`/categories/${id}/`)
}

const create = data => {
  return http.post('/categories', data)
}

const CategoriesService = {
  getAll,
  getById,
  create
}

export default CategoriesService