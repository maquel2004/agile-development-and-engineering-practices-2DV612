import React from 'react'
import Banner from 'Main/components/Banner'
import ContentBox from 'Main/components/ContentBox'
import CheckoutProduct from './components/CheckoutProduct'
import CheckoutTotal from './CheckoutTotal'
import { connect } from 'react-redux'

import './Checkout.css'

const Checkout = ({products, total}) => {
  const prods = products
  return(
    <div className='Checkout'>
      <Banner name='Kassa' />
      <ContentBox>
        <ul>
          {prods && prods.map(item => 
            <CheckoutProduct key={item.product.id +'checkout'} item={item}/>
          )}
        </ul>
        <CheckoutTotal total={total}/>
      </ContentBox>
    </div>
  )
}
  
const mapStateToProps = (state) => {
  return {
    open: state.shoppingCart.open,
    products: state.shoppingCart.products,
    total: state.shoppingCart.total
  }
}

export default connect(
  mapStateToProps
)(Checkout)