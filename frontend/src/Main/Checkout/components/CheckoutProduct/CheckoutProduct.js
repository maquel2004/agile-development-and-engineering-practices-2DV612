import React from 'react'
import './CheckoutProduct.css'
import CheckoutProductInfo from './CheckoutProductInfo'
import CheckoutProductPrice from './CheckoutProductPrice'
import { removeProductFromCart } from 'redux/actions/actions'
import { connect } from 'react-redux'

const CheckoutProduct = ({item, removeProductFromCart}) => {
  const product = item.product

  const handleRemoveProduct = quantity => {
    console.log('wäää')
    console.log(product)
    removeProductFromCart({ quantity, product })
  }

  return(
    <li className='CheckoutProduct'>
      <img src={item.product.image} alt={item.product.name} className='CheckoutProductImage'/>
      <CheckoutProductInfo item={item} />
      <CheckoutProductPrice item={item} action={handleRemoveProduct}/>
    </li>
  )
}


const mapDispatchToProps = { removeProductFromCart }

export default connect(
  null,
  mapDispatchToProps
)(CheckoutProduct)