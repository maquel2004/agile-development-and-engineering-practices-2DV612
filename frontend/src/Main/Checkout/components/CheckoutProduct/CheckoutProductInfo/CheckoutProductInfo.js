import React from 'react'
import './CheckoutProductInfo.css'
import { updateProductInCart } from 'redux/actions/actions'
import { connect } from 'react-redux'

const CheckoutProductInfo = ({item, updateProductInCart}) => {
  const product = item.product

  const handleChange = (e) => {
    handleUpdate(e.target.value)
  }

  const handleUpdate = quantity => {
    updateProductInCart({ quantity, product })
  }

  return(
    <div className='CheckoutProductInfo'>
      <span className='CheckoutProductName'>{item.product.name}</span>
      <span className='CheckoutProductDesc'>{item.product.description}</span>
      <span className='CheckoutProductQuantity'>Antal: <input id='CartProductQuantity' className='CartProductQuantityPicker' type='number' value={item.quantity} min='1' onChange={handleChange}></input></span>
    </div>
  )
}

const mapDispatchToProps = { updateProductInCart }

export default connect(
  null,
  mapDispatchToProps
)(CheckoutProductInfo)