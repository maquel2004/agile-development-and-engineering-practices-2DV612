import React from 'react'

import './CheckoutProductPrice.css'
import './CheckoutProductTrashCan'
import CheckoutProductTrashCan from './CheckoutProductTrashCan'

const CheckoutProductPrice = ({item, action}) =>
  <div className='CheckoutProductPrice'>
    <CheckoutProductTrashCan action={action} quantity={item.quantity} />
    <span className='CheckoutProductTotalPrice'>{item.product.price * item.quantity} kr</span>
  </div>

export default CheckoutProductPrice