import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash } from '@fortawesome/free-solid-svg-icons'

import './CheckoutProductTrashCan.css'

const CheckoutProductTrashCan = ({action, quantity}) =>
  <button className='CheckoutProductTrashCan' onClick={() => { console.log('remove', quantity) 
  action(quantity)}}><FontAwesomeIcon icon={faTrash} className='TrashIcon'/></button>

export default CheckoutProductTrashCan