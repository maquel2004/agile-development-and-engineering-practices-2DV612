import React from 'react'

import './CheckoutTotal.css'

const CheckoutTotal = ({total}) =>
  <div className='CheckoutTotal'>
    <span>Att betala inkl moms</span>
    <span>{total} kr</span>
  </div>

export default CheckoutTotal