import './Start.css'
import { useState, useEffect } from 'react'
import Banner from 'Main/components/Banner'
import ProductCard from 'Main/components/ProductCard'
import ProductsService from 'services/ProductsService'
import ContentBox from 'Main/components/ContentBox'

const Start = () => {
  const [products, setProducts] = useState([])

  useEffect(() => {
    retrieveProducts()
  }, [])

  const retrieveProducts = async () => {
    let response = await ProductsService.getAll()
    const prods = response.data.results
    
    while (response.data.next) {
      const url = response.data.next
      response = await ProductsService.getPage(url.slice(-1))
      prods.push(...response.data.results)
    }
    const prodsSorted  = prods.sort((a, b) => a.created - b.created)
    setProducts(prodsSorted)
  }

  return(
    <div className='Start'>
      <Banner name='Populärt just nu' />
      <Banner name='Senaste produkterna' />
      <ContentBox>
        {products && products.map(item =>   
          <ProductCard key={item.id} product={item} /> 
          )}
      </ContentBox>
    </div>
  )
}

export default Start