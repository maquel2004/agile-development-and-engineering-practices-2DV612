import { useState, useEffect } from 'react'
import Banner from 'Main/components/Banner/index'
import ProductCard from 'Main/components/ProductCard/index'
import './MyProducts.css'
import ProductsService from 'services/ProductsService'
import { Link } from 'react-router-dom'

const MyProducts = () => {
  const [products, setProducts] = useState([])

  useEffect(() => {
    retrieveProducts()
  }, [])

  const retrieveProducts = async () => {
    let response = await ProductsService.getAll()
    const prods = response.data.results
    
    while (response.data.next) {
      const url = response.data.next
      response = await ProductsService.getPage(url.slice(-1))
      prods.push(...response.data.results)
    }
    const prodsSorted  = prods.sort((a, b) => a.created - b.created)
    setProducts(prodsSorted)
  }

  return(
    <div className='MyProducts'>
      <Banner name='Mina Produkter'/>
      <Link id='newProductButton' to='/products/new'>
        <button>Lägg till ny produkt</button>
      </Link>
      <div className='AllMyProducts'>
        {products && products.map(item => 
          <div>
            <ProductCard key={item.id} product={item} />
            <Link to={`/products/${item.id}/edit`}>
              <button>Redigera</button>
            </Link>
          </div>
        )}
      </div>
    </div>
  )
}

export default MyProducts