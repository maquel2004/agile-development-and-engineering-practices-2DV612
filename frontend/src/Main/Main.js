import './Main.css'
import MyProducts from './MyProducts'
import Start from './Start'
import Products from './Products'
import Categories from './Categories'
import EditProduct from './EditProduct'
import NewProduct from './NewProduct'
import Signup from './Signup'
import SignedUp from './SignedUp'
import Flash from './components/Flash'
import Checkout from './Checkout'
import { Switch, Route } from 'react-router-dom'
import TagSearch from './TagSearch'
import ProductSearch from './components/ProductSearch'


const Main = () => 
    <div className='Main'>
      <Flash />
      <Switch>
        <Route exact path='/' component={Start} />
        <Route path='/products/:productId/edit' component={EditProduct} />
        <Route path='/products/new' component={NewProduct} />
        <Route path ='/products' component={Products} />
        <Route path='/myProducts' component={MyProducts} />
        <Route path='/categories' component={Categories} />
        <Route path='/tag-search/:tag' component={TagSearch} />
        <Route path='/search/products/:query' component={ProductSearch} />
        <Route path='/signup' component={Signup} />
        <Route path='/signedup' component={SignedUp} />
        <Route path='/checkout' component={Checkout} />
      </Switch>
    </div>

export default Main