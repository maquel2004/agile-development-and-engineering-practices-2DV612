import Banner from 'Main/components/Banner'
import ContentBox from 'Main/components/ContentBox'
import ProductCard from 'Main/components/ProductCard'
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import ProductsService from 'services/ProductsService'
import './TagSearch.css'

const TagSearch = () => {
  const [products, setProducts] = useState([])
  const params = useParams()
  useEffect(() => {
    retrieveProducts()
  }, [])

  const retrieveProducts = async () => {
    try {
      const response = await ProductsService.getByTag(params.tag)
      setProducts(response.data.results)
    } catch (error) {
      console.error(error)
    }
  }

  return (
    <div className="TagSearch">
      <Banner name={`Produkter taggad med "${params.tag}"`} />
      <ContentBox>
        {products &&
          products.map(item =>
            <ProductCard key={item.id} product={item} />
          )
       }
      </ContentBox>
    </div>
  )

}
export default TagSearch

