import React from 'react'
import './TagContainer.css'

const TagContainer = ({ children }) =>
  <div className="TagContainer">
    {children}
  </div>

export default TagContainer

