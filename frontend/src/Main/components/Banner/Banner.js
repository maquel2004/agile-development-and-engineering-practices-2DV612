import React from 'react'
import './Banner.css'

const Banner = ({ name }) => 
    <div className="Banner">
        <h3>{name}</h3>
    </div>

export default Banner
