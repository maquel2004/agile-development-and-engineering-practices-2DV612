import './ProductShortDesc.css'

const ProductShortDesc = ({desc}) => 
  <span className='ProductShortDesc'>{desc}</span>

export default ProductShortDesc