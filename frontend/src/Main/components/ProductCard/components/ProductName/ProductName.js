import './ProductName.css'

const ProductName = ({name}) => 
  <span className='ProductName'>{name}</span>

export default ProductName