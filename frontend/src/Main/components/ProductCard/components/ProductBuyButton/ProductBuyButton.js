import './ProductBuyButton.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'

const ProductBuyButton = ({price, action}) => 
  <button className='ProductBuyButton' onClick={() => { console.log('clicktest', 1) 
  action(1)}}>{price} kr <FontAwesomeIcon icon={faShoppingCart} className='CartIcon'/></button>

export default ProductBuyButton