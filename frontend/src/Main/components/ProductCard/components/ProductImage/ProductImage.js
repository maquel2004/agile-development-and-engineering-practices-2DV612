import './ProductImage.css'

const ProductImage = ({src, alt}) => 
  <img src={src} alt={alt} className='ProductImageSmall'/>

export default ProductImage