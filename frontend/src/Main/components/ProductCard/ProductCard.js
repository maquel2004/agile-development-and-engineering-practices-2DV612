import React from 'react'
import { NavLink } from 'react-router-dom'
import { connect } from 'react-redux'
import { addProductToCart } from 'redux/actions/actions'
import './ProductCard.css'
import ProductImage from './components/ProductImage'
import ProductName from './components/ProductName'
import ProductShortDesc from './components/ProductShortDesc'
import ProductBuyButton from './components/ProductBuyButton'

const ProductCard = ({ product, addProductToCart}) => {

  const handleAddProduct = quantity => {
    addProductToCart({ quantity, product })
  }

  return(
    <div className='ProductCard'>
      <NavLink to={'/products/' + product.id} className='ProductCardLink'>
        <div className='Product'>
        <ProductImage src={product.image} alt={product.name} />
        <ProductName name={product.name} />
        <ProductShortDesc desc={product.description} />
        </div>
      </NavLink>
      <ProductBuyButton price={product.price} action={ handleAddProduct }/>
    </div>
  )
}

const mapDispatchToProps = { addProductToCart }

export default connect(
  null,
  mapDispatchToProps
)(ProductCard)