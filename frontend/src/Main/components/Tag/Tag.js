import React, { useEffect, useState } from 'react'
import './Tag.css'
import { NavLink } from 'react-router-dom'


const Tag = ({ name, onClick, removable}) => {
  const [onCreate, setOnCreate] = useState(true)
  const [hover, setHover] = useState(false)
  const [classes, setClasses] = useState('Tag')
  const [clicked, setClicked] = useState(false)

  useEffect(() => {
    setTimeout(() => {
      setOnCreate(false)
    }, 1000)
  })
  
  useEffect(() => {
    let classNames = 'Tag'
  
    if (onCreate) classNames += ' onCreate'
    if (removable) classNames += ' removable'

    setClasses(classNames)
  }, [onCreate, removable])

  useEffect(() => {
    if (removable && clicked) {
      setClasses(classes + ' onDelete')
      setTimeout(onClick, 500)
    }
  }, [clicked])

  return (
      <div 
        className={classes}
        onClick={() => setClicked(true)}
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
      >

        {hover && removable &&
          <div className='upperCross' />
        }

        {!removable &&
        <NavLink to={`/tag-search/${name}`} className='TagLink'>
          <label>{name}</label>
        </NavLink>
        } 

        {removable &&
          <label>{name}</label>
        }

        {hover && removable &&
          <hr className='lowerCross' />
        }
    </div>

  )
}
export default Tag

