import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import ProductsService from 'services/ProductsService'
import Banner from '../Banner'
import ContentBox from '../ContentBox'
import ProductCard from '../ProductCard'
import './ProductSearch.css'

const ProductSearch = () => {
  const params = useParams()
  const [products, setProducts] = useState([])

  useEffect(() => {
    retrieveProducts()
  }, [params.query])

  useEffect(() => {
    console.log(products)
  }, [products])
  
  const retrieveProducts = async () => {
    try {
      const response = await ProductsService.search(params.query)
      setProducts(response.data.results)
    } catch (error) {
      
    }
  }
  return (
    <div className="ProductSearch">

      <Banner name={`Sökresultat för "${params.query}"`} />
        <ContentBox>
          {products &&
            products.map(item =>
              <ProductCard key={item.id} product={item} />
            )
         }
        </ContentBox>
    </div>
  )

}
export default ProductSearch

