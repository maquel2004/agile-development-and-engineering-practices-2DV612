import React, { useEffect, useState } from 'react'
import { connect, useDispatch, useSelector } from 'react-redux'
import { setFlash } from 'redux/actions/actions'
import './Flash.css'

const Flash = ({ message, success, visible }) => {
  const dipsatch = useDispatch()

  const visibility = visible ? 'visible' : 'hidden'
  const type = success ? 'success' : 'failure'

  useEffect(() => {
    if (visible) {
      setTimeout(() => {
        dipsatch(setFlash({ visible: false }))
      }, 10 * 1000)
    }
  }, [visible])

  return (
    <div className={`Flash alert-${type} alert-${visibility}`} >
      <span className='messageSpan'>
        {message}
      </span>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    message: state.flash.message,
    success: state.flash.success,
    visible: state.flash.visible
  }
}

export default connect(
  mapStateToProps
)(Flash)

