import React from 'react'
import './ContentBox.css'

const Banner = ({ children }) => 
    <div className='ContentBox'>
        {children}
    </div>

export default Banner
