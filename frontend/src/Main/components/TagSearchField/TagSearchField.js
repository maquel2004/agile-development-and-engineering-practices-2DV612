import React from 'react'
import Input from '../ProductForm/components/Input'
import './TagSearchField.css'

const TagSearchField = ({ value, onChange, onKeyPress, tags }) => {
  const listName = 'tags'
  return (
    <div className="TagSearchField">
      <label>Lägg till taggar</label>

      <input
        list={listName}
        label='Lägg till taggar'
        name='tags'
        value={value}
        onChange={onChange}
        onKeyPress={onKeyPress}
      />

      <datalist id={listName}>
        {tags &&
          tags.map(tag => 
            <option value={tag.name} />)
        }
      </datalist>
    </div>
  )
}

export default TagSearchField

