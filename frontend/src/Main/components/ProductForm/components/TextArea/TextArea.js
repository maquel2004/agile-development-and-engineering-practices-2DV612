import React from 'react'
import './TextArea.css'

const TextArea = ({ label, name, type, value, onChange }) =>
  <div className="TextArea">
    <label>
      {label}
    </label>

    <textarea
      type={type}
      name={name}
      value={value}
      onChange={onChange}
      required={true}
    />
  </div>

export default TextArea

