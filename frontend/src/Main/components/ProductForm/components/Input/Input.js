import React from 'react'
import './Input.css'

const Input = ({ label, name, type, value, onChange, onKeyPress, required = true }) =>
  <div className="Input">
    <label>
      {label}
    </label>

    <input
      type={type}
      name={name}
      value={value}
      onChange={onChange}
      onKeyPress={onKeyPress}
      min='1'
      required={required}
    />
  </div>

export default Input

