import React from 'react'
import './Image.css'

const Image = ({ source, name }) =>
  <div className='Image'>
    <img src={source} alt={name}/>
  </div>

export default Image

