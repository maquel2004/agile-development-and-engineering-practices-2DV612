import React from 'react'
import './ImageUpload.css'

const ImageUpload = ({ children, label, name, files, onChange }) =>
  <div className="ImageUpload">
    <label>
      {label}
    </label>

    {children}

    <input
      type='file'
      accept='image/*'

      name={name}
      files={files}
      onChange={onChange}
    />
  </div>

export default ImageUpload

