import React from 'react'
import './Dropdown.css'

const Dropdown = ({ label, name, value, children, onChange}) =>
  <div className="Dropdown">
    <label>
      {label}
    </label>
    
    <select name={name} value={value} onChange={onChange}>
      {children}      
    </select>
  </div>

export default Dropdown

