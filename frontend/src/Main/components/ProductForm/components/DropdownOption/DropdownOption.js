import React from 'react'
import './DropdownOption.css'

const DropdownOption = ({ value, name}) =>
  <option className="DropdownOption" value={value}>
    {name}
  </option>

export default DropdownOption

