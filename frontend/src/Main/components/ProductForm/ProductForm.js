import React, { useEffect, useState } from 'react'
import ProductsService from 'services/ProductsService'
import CategoriesService from 'services/CategoriesService'
import './ProductForm.css'
import Input from './components/Input'
import TextArea from './components/TextArea'
import ImageUpload from './components/ImageUpload'
import Dropdown from './components/Dropdown'
import DropdownOption from './components/DropdownOption'
import Image from './components/Image'
import { Redirect } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { setFlash } from 'redux/actions/actions'
import Tag from '../Tag'
import TagContainer from '../TagContainer'
import TagsService from 'services/TagsService'
import TagSearchField from '../TagSearchField'
import { API_ENDPOINT } from 'config/url'

const ProductForm = ({ productId }) => {
  const dispatch = useDispatch()

  const [redirect, setRedirect] = useState(false)
  const [name, setName] = useState('')
  const [description, setDescription] = useState('')
  const [price, setPrice] = useState(1)

  const [quantity, setQuantity] = useState(1)
  const [image, setImage] = useState('')
  const [category, setCategory] = useState([])

  const [tags, setTags] = useState([])
  const [tagInput, setTagInput] = useState('')
  const [tagSearchResults, setTagSearchResults] = useState(null)

  const [product, setProduct] = useState({})
  const [tempProduct, setTempProduct] = useState({})
  const [categories, setCategories] = useState([])

  const reader = new FileReader()
  reader.onload = () => setImage(reader.result)

  useEffect(() => {
    const retrieveProduct = async() => {
      try {
        const product = await ProductsService.getById(productId)
                                             .then(response => response.data)
        updateProduct(product)
      } catch (error) {
        console.error('Hoppsan! Något gick fel när produkten skulle hämtas')
      }
    }

    const retrieveCategories = async () => {
      try {
        const response = await CategoriesService.getAll()
        setCategories(response.data.results)
      } catch (error) {
        console.log('Nu gick någonting fel när kategorierna skulle hämtas')
      }
    }

    const updateProduct = (product) => {
      setTempProduct(product)
    
      setName(product.name)
      setDescription(product.description)
      setPrice(product.price)
      
      setQuantity(product.quantity)
      setImage(product.image)
      setCategory(product.category)
    }

    if (productId) {
     retrieveProduct()  
    }
    retrieveCategories()
  }, [productId])

  useEffect(() => {
    const sendData = async () => {
      try {
        const response = await sendDataAndReturnResponse()
        handleResponse(response)
        setRedirect(true)
      } catch (error) {
        // If the error message contains a status code (400, 500 and so on) the message will be set to a default value to prevent writing something like "internal server error" in the flash message
        // TODO handle errors in backend. For now if the product already exists, the server answers with code 500
        const errorMessage = !/\d/.test(error.message) ? error.message : 'Något gick fel, försök igen senare'
        dispatch(setFlash({message: errorMessage, success: false, visible: true}))
      }
    }

    const sendDataAndReturnResponse = async () => {
      const response = productId
                     ? await ProductsService.update(product)
                     : await ProductsService.create(product)
  
      return response
    }
  
    const handleResponse = (res) => {
      switch (res.status) {
        case 200: dispatch(setFlash({ message: 'Produkten uppdaterad', success: true }))
          break
        case 201: dispatch(setFlash({ message: 'Produkten skapad', success: true }))
          break
        default:
          const action = productId ? 'uppdatera' : 'skapa'
          throw new Error(`Det gick inte att ${action} produkten, försök igen senare`)
        }      
    }

    // To prevent sendData() on page load
    if(Object.keys(product).length === 0) return
    sendData()

    window.scrollTo(0, 0)
  }, [product, productId])

  const submitHandler = async (event) => {
    event.preventDefault()

    setProduct({
      ...tempProduct,
      name: name,
      description: description,
      price: price,
      quantity: quantity,
      image: image,
      category: category[0] ? category : [event.target.category.value],
      tags: tags.map(tag =>
        `${API_ENDPOINT}/tags/${tag.id}/`  
      )
    })
  }

  useEffect(() => {
    if (tempProduct.tags) {
      retrieveTags()
    }
  }, [tempProduct])

  const retrieveTags = async () => {
    try {
      setTags(
        await Promise.all(
          tempProduct.tags.map(async url =>
            await TagsService.getFromUrl(url).then(res => res.data)
        )))  
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    const updateTags = async () => {
      try {
        setTagSearchResults(
          await TagsService.search(tagInput.toLowerCase())
                           .then(response => response.data.results)
        )
      } catch (error) {
        console.error(error)
      }
    }

    if (tagInput.length > 1) {
      updateTags()
    }
  }, [tagInput])

  const addTag = async (e) => {
    if (e.code !== 'Space' && e.code !== 'Enter') return

    e.preventDefault()
    const tag = tagSearchResults.filter(tag => tag.name === tagInput)
    
    if (tag.length === 1) {
      setTags([...tags, ...tag])
    } else {
      try {
        const res = await TagsService.create({ name: tagInput })
        setTags([...tags, res.data])
      } catch (error) {
        console.error(error)
      }
    }

    setTagInput('')
  }

  return (
    <div>
      {redirect && <Redirect to='/myproducts' />}
      <form className='ProductForm' onSubmit={submitHandler}>

        <Input
          label='Produktnamn'
          name='name'
          type='text'
          value={name}
          onChange={(e) => setName(e.target.value)}
        />

        <TextArea
          label='Produktbeskrivning'
          name='description'
          type='text'
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />

        <TagSearchField
          value={tagInput}
          onChange={(e) => setTagInput(e.target.value.toLowerCase())}
          tags={tagSearchResults}
          onKeyPress={addTag}
        />
       
        <TagContainer>
        {tags && tags.map(tag =>
          <Tag
            name={tag.name}
            key={tag.id}
            removable={true}
            onClick={(e) => {
              setTags(tags.filter(tags => tags !== tag))
            }}
          />
        )}
        </TagContainer>

        <Input
          label='Pris'
          name='price'
          type='number'
          value={price}
          onChange={(e) => setPrice(e.target.value)}
        />

        <Input
          label='Kvantitet'
          name='quantity'
          type='number'
          value={quantity}
          onChange={(e) => setQuantity(e.target.value)}
        />

        <ImageUpload
          label='Bild'
          name='image'
          files={image}
          onChange={(e) => reader.readAsDataURL(e.target.files[0])}
        >

          <Image source={image} name={name} />

        </ImageUpload>

        <Dropdown label='Kategori'
          name='category'
          value={category[0]}
          onChange={(e) => setCategory([e.target.value])}
        >

          {categories && categories.map(category =>
            <DropdownOption
              key={category.id}
              value={`http://localhost:8000/api/categories/${category.id}/`}
              name={category.name}
            />
          )}

        </Dropdown>

        <input className='submit' type="submit" value={productId ? 'Uppdatera' : 'Lägg till'} />
      </form>
    </div>
  )
}

export default ProductForm

