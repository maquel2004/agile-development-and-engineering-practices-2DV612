import React from 'react'
import { useParams } from 'react-router-dom'
import './EditProduct.css'
import ProductForm from 'Main/components/ProductForm'
import Banner from 'Main/components/Banner'

const EditProduct = () => {
let params = useParams()

  return (
    <div className="EditProduct">
      <Banner name='Redigera produkt' />
      <ProductForm productId={params.productId}/>
    </div>
  )
}
export default EditProduct

