import './SignedUp.css'
import React, { Component } from 'react';
import { Redirect } from "react-router-dom";
import axiosInstance from '../../auth-common';
import { useState, useEffect } from 'react'

class SignedUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            signedIn: this.isUserLoggedIn(),
        };
    }

    isUserLoggedIn() {
        if (localStorage.getItem('access_token') == null) {
            return false;
        } else {
            this.props.history.push("/");
            return true;
        }
    }

    render() {
        return (
            <div className={this.state.signedIn ? 'hidden' : ''}>
                Tack för att du registrerade ett konto! Du kan nu logga in med det angivna användarnamnet
                och lösenordet.
            </div>
        )
    }
}

export default SignedUp
