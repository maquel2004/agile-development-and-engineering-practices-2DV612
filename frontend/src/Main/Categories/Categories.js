import './Categories.css'
import { useEffect, useState } from 'react'
import { Switch, Route, useParams, useRouteMatch } from 'react-router-dom'
import CategoriesService from 'services/CategoriesService'
import ProductsService from 'services/ProductsService'
import Banner from 'Main/components/Banner'
import ContentBox from 'Main/components/ContentBox'
import ProductCard from 'Main/components/ProductCard'

const Categories = () => {

  let match = useRouteMatch()

  return(
    <Switch>
        <Route path={`${match.path}/:categoryId`}>
          <Category />
        </Route>
        <Route path={match.path}>
          Hoppsan! Du måste ange id för en kategori.
        </Route>
      </Switch>
  )
}

const Category = () => {
  let params = useParams()
  const [category, setCategory] = useState({})
  const [products, setProducts] = useState([])

  useEffect(() => {
    retrieveProducts()
  }, [params.categoryId])

  const retrieveCategory = async () => {
    try {
      const response = await CategoriesService.getById(params.categoryId)
      setCategory(response.data)
    } catch (err) {
      setCategory({error: 'Hoppsan! Denna kategori existerar inte.'})
    }
  }

  const retrieveProducts = async () => {
    try {
      const response = await ProductsService.getByCategory(params.categoryId)
      setProducts(response.data.results)
    } catch (error) {
      
    }
  }

  return(
    <div className='Category'>
      <Banner name={`Produkter inom kategorin "${params.categoryId}"`} />
      <ContentBox>
        {products &&
          products.map(item =>
            <ProductCard key={item.id} product={item} />
          )
       }
      </ContentBox>
    </div>
  )
}

export default Categories