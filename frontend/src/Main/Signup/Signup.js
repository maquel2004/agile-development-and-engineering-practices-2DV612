import './Signup.css'
import React, { Component } from 'react';
import { Redirect } from "react-router-dom";
import axiosInstance from '../../auth-common';
import { useState, useEffect } from 'react'

class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            producer: false,
            errors: {},
            hiddenRegistrationForm: this.isUserLoggedIn(),
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    isUserLoggedIn() {
        if (localStorage.getItem('access_token') == null) {
            return false;
        } else {
            this.props.history.push("/");
            return true;
        }
    }

    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    onChangeProducer = () => {
        this.setState(initialState => ({
          producer: !initialState.producer,
        }));
    }

    async handleSubmit(event) {
        event.preventDefault();
        try {
            const response = await axiosInstance.post('/users/create', {
                user: {
                    username: this.state.username,
                    password: this.state.password,
                    is_producer: this.state.producer,
                },
            });
            this.props.history.push("/signedup");
            return response;
        } catch (error) {
            console.log(error.stack);
            this.setState({
                errors:error.response.data
            });
        }
    }

    render() {
        return (
            <div id="signup-content" className={this.state.hiddenRegistrationForm ? 'hidden' : ''}>
             Kontoregistrering
                <form id="signup-form" onSubmit={this.handleSubmit}>
                    <label>
                        Användarnamn:
                        <input name="username" type="text" value={this.state.username} onChange={this.handleChange}/>
                        { this.state.errors.username ? this.state.errors.username : null}
                    </label>
                    <label>
                        Lösenord:
                        <input name="password" type="password" value={this.state.password} onChange={this.handleChange}/>
                        { this.state.errors.password ? this.state.errors.password : null}
                    </label>
                    <label className="form-check-label">
                     <input type="checkbox"
                        checked={this.state.producer}
                        onChange={this.onChangeProducer}
                        className="form-check-input"/>
                      Registrera som producent
                    </label>
                    <input type="submit" value="Registrera"/>
                </form>
            </div>
        )
    }
}

export default Signup
