import './MetaDescription.css'

const MetaDescription = ({text}) =>
  <div className='MetaDescription'>
    <span className='DescriptionHeader'>
      Beskrivning
    </span>
    <div className='DescriptionText'>
      {text}
    </div>
  </div>

export default MetaDescription