import './MetaProducer.css'
import { NavLink } from 'react-router-dom'

const MetaProducer = ({name}) => 
  <div className='MetaProducer'>
    Försäljare: <NavLink to='/producers/id' className='MetaProducerLink'>{name}</NavLink>
  </div>

  export default MetaProducer