import './CartButton.css'

const CartButton = ({ quantity, action }) => 
  <button className='CartButton' onClick={() => { console.log('clicktest', quantity) 
  action(quantity)}}>
    LÄGG I KUNDVAGN
  </button>

export default CartButton