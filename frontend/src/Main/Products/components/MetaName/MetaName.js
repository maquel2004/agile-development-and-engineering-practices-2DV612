import './MetaName.css'

const MetaName = ({name}) => 
  <div className='MetaName'>
    {name} <span className='MetaCatchPhrase'>- ekologiska</span>
  </div>

export default MetaName