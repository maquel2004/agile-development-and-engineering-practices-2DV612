import './MetaWishAndShip.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHeart, faTruck } from '@fortawesome/free-solid-svg-icons'

const MetaWishAndShip = () => 
  <div className='MetaWishAndShip'>
    <div className='MetaWish'>
      <FontAwesomeIcon icon={faHeart} className='WishIcon'/>
      <span className='WishText'>Spara till önskelista</span>
    </div>
    <div className='MetaShip'>
      <FontAwesomeIcon icon={faTruck} className='WishIcon'/>
      <div className='ShippingInfo'>
        <span className='Days'>Skickas inom 3-5 vardagar</span>
        <span className='ExtraInfo'>Fri frakt från 199 kr</span>
      </div>
    </div>
  </div>

  export default MetaWishAndShip