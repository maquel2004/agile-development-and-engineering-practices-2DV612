import './MetaQuantity.css'
import CartButton from '../CartButton'
import { useState } from 'react';

const MetaQuantity = ({ action }) => {
  const [input, setInput] = useState(1)

  return (
  <div className='MetaQuantity'>
  <div className='MetaPickQuantity'>
    <span className='MetaQuantityText'>
      Välj kvantitet
    </span>
    <input id='quantity' className='MetaPicker' type='number' defaultValue='1' min='1' onInput={e => setInput(e.target.value)}></input>
  </div>
  <CartButton quantity={input} action={ action }/>
  </div>
  )
}
export default MetaQuantity