import './MetaPrice.css'

const MetaPrice = ({price}) =>
  <div className='MetaPrice'>
    <span className='MetaPricePoint'>{price} kr</span> / kilot
  </div>

export default MetaPrice