import './Products.css'
import { useEffect, useState } from 'react'
import { Switch, Route, useParams, useRouteMatch, NavLink } from 'react-router-dom'
import { connect } from 'react-redux'
import { addProductToCart } from 'redux/actions/actions'

import MetaData from './components/MetaData'
import MetaName from './components/MetaName'
import MetaProducer from './components/MetaProducer'
import MetaPrice from './components/MetaPrice'
import MetaQuantity from './components/MetaQuantity'
import MetaWishAndShip from './components/MetaWishAndShip'
import MetaDescription from './components/MetaDescription'
import ProductsService from 'services/ProductsService'
import TagContainer from 'Main/components/TagContainer'
import Tag from 'Main/components/Tag'
import TagsService from 'services/TagsService'

const Products = ({ addProductToCart }) => {
  
  let match = useRouteMatch()
  
  return(
    <Switch>
        <Route path={`${match.path}/:productId`}>
          <Product action={ addProductToCart } />
        </Route>   
        <Route path={match.path}>
          Hoppsan! Du måste ange ett produkt id.
        </Route>
      </Switch>
  )
}

const Product = ({ action }) => {
  let params = useParams()
  const [product, setProduct] = useState({})
  const [tags, setTags] = useState([])

  useEffect(() => {
    retrieveProduct()
  }, [])

  useEffect(() => {
    if (product.tags) {
      retrieveTags()
    }
}, [product])

  const retrieveProduct = async () => {
    try {
      const response = await ProductsService.getById(params.productId)
      setProduct(response.data)
    } catch (err) {
      setProduct({error: 'Hoppsan! Denna produkt existerar inte.'})
    }
  }

  const retrieveTags = async () => {
    try {
      setTags(
        await Promise.all(
          product.tags.map(async url =>
            await TagsService.getFromUrl(url).then(res => res.data)
        )))  
    } catch (error) {
      console.log(error)
    }
  }

  const handleAddProduct = quantity => {
    action({ quantity, product })
  }

  return(
    <div className='Product'>
      {product.error}
      <div className='ProductImage'>
        <img alt={product.name} src ={product.image}/>
      </div>
      <MetaData>
        <MetaName name={product.name} />
        <MetaProducer name='Växjö Grönt'/>
        <MetaPrice price={product.price} />
        <MetaQuantity action={ handleAddProduct }/>
        <MetaWishAndShip />
        <MetaDescription text={product.description} />
        <TagContainer>
          {tags && tags.map(tag =>
            <Tag name={tag.name} key={tag.id} />
          )}
        </TagContainer>
      </MetaData>
    </div>
  )
}

const mapDispatchToProps = { addProductToCart }

export default connect(
  null,
  mapDispatchToProps
)(Products)