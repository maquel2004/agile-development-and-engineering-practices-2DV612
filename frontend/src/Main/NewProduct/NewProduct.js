import React from 'react'
import './NewProduct.css'
import ProductForm from 'Main/components/ProductForm'
import Banner from 'Main/components/Banner'

const NewProduct = () => {
  return (
    <div className="NewProduct">
      <Banner name='Lägg till ny produkt' />
      <ProductForm />
    </div>
  )
}

export default NewProduct

