cd "$(dirname "$0")"	
DB_FILE=db.sqlite3
if [ ! -f "$DB_FILE" ]; then
    echo "$DB_FILE missing - generate..."
    sqlite3 $DB_FILE < db.sql
fi