from rest_framework import permissions
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView

from ADEP.serializers import GetFullUserSerializer, UserSerializerWithToken


@api_view(['GET'])
def get_current_user(request):
    """Gets the current User."""
    serializer = GetFullUserSerializer(request.user)
    return Response(serializer.data)


class CreateUserView(APIView):
    """
    APIView for Creating Users.
    """
    permission_classes = (permissions.AllowAny, )

    def post(self, request):
        user = request.data.get('user')
        if not user:
            return Response({'response': 'error', 'message': 'No data found'})
        serializer = UserSerializerWithToken(user['is_producer'], data=user)
        if serializer.is_valid():
            saved_user = serializer.save()
        else:
            return Response({'response': 'error', 'message': serializer.errors})
        return Response({'response': 'success', 'message': 'user created succesfully'})