from django.contrib import admin
from .models import Producer

# Register producer to admin site
admin.site.register(Producer)
