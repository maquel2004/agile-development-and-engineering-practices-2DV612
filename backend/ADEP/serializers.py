from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework_jwt.settings import api_settings

from producers.models import Producer


class GetFullUserSerializer(serializers.ModelSerializer):
    """
    Serializing a User object.
    """
    class Meta:
        model = User
        fields = ('id', 'username', 'is_superuser', 'first_name', 'last_name', 'producer')


class UserSerializerWithToken(serializers.ModelSerializer):
    """
    Serializing a User token.
    """
    password = serializers.CharField(write_only=True)
    token = serializers.SerializerMethodField()

    def __init__(self, is_producer, data, **kwargs):
        super().__init__(data=data, **kwargs)
        self.is_producer = is_producer

    def get_token(self, object):
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
        payload = jwt_payload_handler(object)
        token = jwt_encode_handler(payload)
        return token

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
        )
        user.set_password(validated_data['password'])
        user.save()

        if self.is_producer:
            producer = Producer.objects.create(user=user)
            producer.save()
        return user

    class Meta:
        model = User
        fields = ['token', 'username', 'password']

