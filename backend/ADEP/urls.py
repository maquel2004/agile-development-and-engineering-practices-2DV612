from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token

from ADEP.views import get_current_user, CreateUserView
from orders.views import OrderViewSet, OrderRowViewSet
from products.views import ProductViewSet, CategoryViewSet, TagViewSet,\
                           ProductSearchView, TagSearchView, TagSearchExactView,\
                           ProductsByTagSearchView, ProductsByCategorySearchView


router = routers.DefaultRouter()
router.register(r'products', ProductViewSet)
router.register(r'categories', CategoryViewSet)
router.register(r'tags', TagViewSet)
router.register(r'orders', OrderViewSet)
router.register(r'orderrows', OrderRowViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/current_user/', get_current_user),
    path('api/users/create', CreateUserView.as_view()),
    path('api/token/create', obtain_jwt_token),
    path('api/search-products/', ProductSearchView.as_view()),
    path('api/search-products-by-category/', ProductsByCategorySearchView.as_view()),
    path('api/search-products-by-tag/', ProductsByTagSearchView.as_view()),
    path('api/search-tags/', TagSearchView.as_view()),
    path('api/search-tags-exact/', TagSearchExactView.as_view()),
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls'))
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
