from django.db.models import Q
from rest_framework import viewsets, generics, filters
from rest_framework import permissions
from rest_framework.decorators import api_view
from products.models import Product, Category, Tag
from products.serializers import ProductSerializer, CategorySerializer, TagSerializer


@api_view(['GET'])
def process_request(request):
    print(request.body)
    return None


class DynamicSearchFilter(filters.SearchFilter):
    def get_search_fields(self, view, request):
        return request.GET.getlist('search_fields', [])


class ExactSearchFilter(filters.SearchFilter):
    def get_search_fields(self, view, request):
        return request.GET.getlist('search_fields')


class CategoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows categories to be viewed or edited.
    """
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = [permissions.AllowAny]


class TagViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows tags to be viewed or edited
    """
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    permission_classes = [permissions.AllowAny]


class ProductViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows products to be viewed or edited.
    """
    queryset = Product.objects.all().order_by('-created')
    serializer_class = ProductSerializer
    permission_classes = [permissions.AllowAny]


class ProductSearchView(generics.ListCreateAPIView):
    """
    API endpoint that allows searching for products.
    """
    search_fields = ['name', 'description']
    ordering_fields = ['category']
    filter_backends = (DynamicSearchFilter,)
    serializer_class = ProductSerializer
    permission_classes = [permissions.AllowAny]

    def get_queryset(self):
        query = self.request.GET.get('search')
        object_list = Product.objects.filter(
            Q(name__icontains=query) |
            Q(description__icontains=query) |
            Q(category__name__icontains=query) |
            Q(tags__name__icontains=query)
        )
        return list(set(object_list))


class ProductsByCategorySearchView(generics.ListCreateAPIView):
    """
    API endpoint that allows searching for products by Category
    """
    search_fields = ['name']
    filter_backends = (DynamicSearchFilter,)
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [permissions.AllowAny]

    def get_queryset(self):
        query = self.request.GET.get('search')
        object_list = Product.objects.filter(
            Q(category__name__iexact=query) |
            Q(category__id__iexact=query)
        )
        return object_list


class TagSearchView(generics.ListCreateAPIView):
    """
    API endpoint that allows searching for Tags.
    """
    search_fields = ['name']
    filter_backends = (DynamicSearchFilter,)
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    permission_classes = [permissions.AllowAny]

    def get_queryset(self):
        query = self.request.GET.get('search')
        object_list = Tag.objects.filter(
            Q(name__icontains=query)
            #Q(name__istartswith=query)
        )
        return object_list


class TagSearchExactView(generics.ListCreateAPIView):
    """
    API endpoint that allows searching for a specific Tag.
    """
    search_fields = ['=name']
    filter_backends = (DynamicSearchFilter,)
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    permission_classes = [permissions.AllowAny]

    def get_queryset(self):
        query = self.request.GET.get('search')
        object_list = Tag.objects.filter(
            Q(name__iexact=query)
            #Q(name__istartswith=query)
        )

        return object_list


class ProductsByTagSearchView(generics.ListCreateAPIView):
    """
    API endpoint that allows searching for a specific Tag.
    """
    search_fields = ['tags']
    filter_backends = (DynamicSearchFilter,)
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [permissions.AllowAny]

    def get_queryset(self):
        query = self.request.GET.get('search')
        object_list = Product.objects.filter(
            Q(tags__name__iexact=query) |
            Q(tags__id__iexact=query)
        )

        return object_list
