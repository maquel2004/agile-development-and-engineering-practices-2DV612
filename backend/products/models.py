from django.db import models
from django.utils.text import slugify
from producers.models import Producer


class Category(models.Model):
    """
    Represents a Product.
    """
    name = models.CharField(verbose_name='Namn', max_length=255)
    description = models.TextField(verbose_name='Beskrivning')
    slug = models.SlugField(unique=True, blank=True, editable=False)

    def save(self, *args, **kwargs):
        self.slug = self.slug or slugify(self.name)
        super().save(*args, **kwargs)


class Tag(models.Model):
    """
    Represents a Tag
    """
    name = models.CharField(verbose_name='name', max_length=255)
    slug = models.SlugField(unique=True, blank=True, editable=False)

    def save(self, *args, **kwargs):
        self.name = self.name.lower()
        self.slug = self.slug or slugify(self.name)
        super().save(*args, **kwargs)
        print(self)


class Product(models.Model):
    """
    Represents a Product.
    """
    name = models.CharField(verbose_name='Namn', max_length=255)
    description = models.TextField(verbose_name='Beskrivning')
    image = models.TextField(verbose_name='Bild', null=True, blank=True)  # Sätta default bild enligt kategori
    price = models.FloatField(verbose_name='Pris')
    quantity = models.PositiveIntegerField(verbose_name='Kvantitet')
    category = models.ManyToManyField(Category)
    tags = models.ManyToManyField(Tag)
    producer = models.ForeignKey(Producer, null=True, on_delete=models.CASCADE)
    created = models.DateTimeField(verbose_name='Skapad', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='Modiferad', auto_now=True)
    slug = models.SlugField(unique=True, blank=True, editable=False)

    def save(self, *args, **kwargs):
        self.slug = self.slug or slugify(self.name)
        super().save(*args, **kwargs)

