from rest_framework import serializers
from products.models import Category, Tag, Product


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializing a Category object.
    """
    class Meta:
        model = Category
        fields = ['id', 'name', 'description', 'slug']


class TagSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializing a Tag object
    """
    class Meta:
        model = Tag
        fields = ['id', 'name', 'slug']


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializing a Product object.
    """
    class Meta:
        model = Product
        fields = ['id', 'name', 'description', 'image', 'price',
                  'quantity', 'category', 'tags', 'created', 'updated', 'slug']

