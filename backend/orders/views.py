from rest_framework import viewsets
from rest_framework import permissions

from orders.models import Order, OrderRow
from orders.serializers import OrderSerializer, OrderRowSerializer


class OrderViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows orders to be viewed or edited.
    """
    queryset = Order.objects.all().order_by('-created')
    serializer_class = OrderSerializer
    permission_classes = [permissions.IsAuthenticated]


class OrderRowViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows order rows to be viewed or edited.
    """
    queryset = OrderRow.objects.all().order_by('-created')
    serializer_class = OrderRowSerializer
    permission_classes = [permissions.IsAuthenticated]
