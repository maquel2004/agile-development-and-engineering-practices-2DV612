from rest_framework import serializers
from orders.models import Order, OrderRow


class OrderSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializing an Order object.
    """
    class Meta:
        model = Order
        fields = ['id', 'status', 'created', 'updated']


class OrderRowSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializing an Order Row object.
    """
    class Meta:
        model = OrderRow
        fields = ['id', 'sum', 'quantity', 'created', 'updated', 'product_id', 'order_id']
