from django.db import models
from products.models import Product


class Order(models.Model):
    """
    Represents an Order.
    """
    # Predefined status tuple
    STATUSES = (
        ('open', 'Open'),
        ('closed', 'Closed'),
    )
    status = models.CharField(verbose_name='Status', max_length=10, choices=STATUSES, default='open')
    created = models.DateTimeField(verbose_name='Skapad', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='Modiferad', auto_now=True)
    # TODO: Foreign keys


# Purchase order row
class OrderRow(models.Model):
    """
    Represents an Order Row.
    Row sum (quantity * current product price).
    """
    sum = models.DecimalField(default=0.0, max_digits=9, decimal_places=2)
    quantity = models.PositiveIntegerField(default=0)
    created = models.DateTimeField(verbose_name='Skapad', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='Modiferad', auto_now=True)

    # Foreign keys (should remove id on the field name, in db e.g it's product_id_id now)
    product_id = models.ForeignKey(Product, null=True, on_delete=models.SET_NULL)
    order_id = models.ForeignKey(Order, on_delete=models.CASCADE)
