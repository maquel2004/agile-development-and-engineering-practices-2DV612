from django.db import models
from django.contrib.auth.models import User


class Producer(models.Model):
    """
    Represents a Producer.
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    presentation = models.TextField(verbose_name='Presentation', default='Fyll i presentation')
    phone = models.CharField(verbose_name="Telefon", max_length=16, default='Fyll i telefonnummer')
    street = models.CharField(verbose_name="Gatuaddress", max_length=64, default='Fyll i gatuadress')
    postal_nr = models.CharField(verbose_name="Postnummer", max_length=64, default='Fyll i postnummer')
    postal_address = models.CharField(verbose_name="Postort", max_length=64, default='Fyll i postort')

    logo = models.ImageField(upload_to='media/products', null=True, blank=True)
