# Generated by Django 3.1.3 on 2020-12-29 10:58

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Producer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('presentation', models.TextField(verbose_name='Presentation')),
                ('phone', models.CharField(max_length=16, verbose_name='Telefon')),
                ('street', models.CharField(max_length=64, verbose_name='Gatuaddress')),
                ('postal_nr', models.CharField(max_length=64, verbose_name='Postnummer')),
                ('postal_address', models.CharField(max_length=64, verbose_name='Postort')),
                ('logo', models.ImageField(blank=True, null=True, upload_to='media/products')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
