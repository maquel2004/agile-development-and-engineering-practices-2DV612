# Agile development and engineering practices - 2DV612

## About

The LNU course Agile development and engineering practices.

## Technologies

- Django 3.1.3 (backend solution)
- Django Rest Framework (RESTful service)
- SQLite 3 (DB)
- React 17.0.1 (frontend solution)

## Team

Marcus Cvjeticanin - [@mjovanc](https://github.com/mjovanc)\
Johan Andersson - [@brocahontaz](https://github.com/brocahontaz)\
Emil Solin - [@solqu](https://github.com/solqu)\
Niklas Hägg - [@Nevland1](https://github.com/Nevland1)\
Peter Nordlander - [@xorself](https://github.com/xorself)
